<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST' || !isset($_POST['clasa']))
    redirect("error.php?id=nepermis");
$clasa=$_POST['clasa'];
$extensii=get_extensii();
$user=$_SESSION['user'];
if (!$_FILES['fisier']['name'])
{
    opensession('error', '<font color="red">Nu ati incarcat niciun fisier!</font>');
    redirect("incarca.php");
}
if ($_FILES['fisier']['error'])
{
    opensession('error', '<font color="red">'.$_FILES['fisier']['error'].'</font><br/>');
    redirect("incarca.php");
}
$nume=mres($_FILES['fisier']['name']);
$file_parts = @pathinfo($nume);
$size=$_FILES['fisier']['size']/1024;
if (!in_array(strtolower($file_parts['extension']), $extensii))
{
    opensession('error', '<font color="red">Extensie neacceptata!</font><br/>');
    redirect("incarca.php");
}
if (bad_words($nume))
{
    opensession('error', '<font color="red">Numele fisierului contine cuvinte obscene!</font><br/>');
    redirect("incarca.php");
}
if ($_FILES['fisier']['size']>get_dim_max()*1024000 && rank($_SESSION['user'])=='E')
{
    opensession('error', '<font color="red">Fisierul este prea mare!</font>');
    redirect("incarca.php");
}
if (query("SELECT * FROM fisiere WHERE nume='$nume'"))
{
    opensession('error', '<font color="red">Fisierul exista deja! Redenumiti-l!</font>');
    redirect("incarca.php");
}
move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
@mysql_query("INSERT INTO fisiere (nume, cale, marime, user, clasa) VALUES ('$nume', 'http://email.colegiulghica.ro/uploads/$nume', $size, '$user', '$clasa')");
opensession('succes', 'Fisierul a fost incarcat cu succes!');
if ($clasa=='ALL') 
{
    $rez=@query("SELECT id FROM fisiere WHERE nume='$nume'");
    $id=$rez[0];
    opensession('link', 'Pentru a-l descarca, accesati urmatorul link: <br/>http://email.colegiulghica.ro/descarca.php?id='.$id);
}
increment_uploads($_SESSION['user']);
redirect("succes.php");
?>