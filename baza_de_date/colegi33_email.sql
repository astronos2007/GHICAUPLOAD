-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Gazda: localhost
-- Timp de generare: 15 Iul 2013 la 16:57
-- Versiune server: 5.0.92-community-log
-- Versiune PHP: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `colegi33_email`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `anunturi`
--

CREATE TABLE IF NOT EXISTS `anunturi` (
  `id` int(11) NOT NULL auto_increment,
  `anunt` text NOT NULL,
  `clasa` varchar(3) NOT NULL,
  `user` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `clase`
--

CREATE TABLE IF NOT EXISTS `clase` (
  `id` int(11) NOT NULL auto_increment,
  `clasa` varchar(3) NOT NULL,
  `nr_elevi` smallint(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `cnp`
--

CREATE TABLE IF NOT EXISTS `cnp` (
  `id` int(11) NOT NULL auto_increment,
  `cnp` varchar(23) NOT NULL,
  `utilizat` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `fundaluri`
--

CREATE TABLE IF NOT EXISTS `fundaluri` (
  `id` int(11) NOT NULL auto_increment,
  `back` text NOT NULL,
  `d_color` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ip`
--

CREATE TABLE IF NOT EXISTS `ip` (
  `ip` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mesaje`
--

CREATE TABLE IF NOT EXISTS `mesaje` (
  `id` int(11) NOT NULL auto_increment,
  `user` varchar(30) NOT NULL,
  `mesaj` text NOT NULL,
  `de_la` text NOT NULL,
  `subiect` text NOT NULL,
  `data` datetime NOT NULL,
  `atasament` text NOT NULL,
  `nume_att` varchar(30) NOT NULL,
  `citit` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `setari`
--

CREATE TABLE IF NOT EXISTS `setari` (
  `dim_max` bigint(20) NOT NULL,
  `max_thumb` bigint(20) NOT NULL,
  `extensii` text NOT NULL,
  `blocked_emails` tinyint(1) NOT NULL,
  `blocked_uploads` tinyint(1) NOT NULL,
  `blocked_shoutbox` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `shoutbox`
--

CREATE TABLE IF NOT EXISTS `shoutbox` (
  `id` int(11) NOT NULL auto_increment,
  `mesaj` text NOT NULL,
  `user` varchar(30) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `trimise`
--

CREATE TABLE IF NOT EXISTS `trimise` (
  `id` int(11) NOT NULL auto_increment,
  `user` varchar(30) NOT NULL,
  `subiect` varchar(40) NOT NULL,
  `mesaj` text NOT NULL,
  `destinatar` varchar(40) NOT NULL,
  `atasament` text NOT NULL,
  `nume_att` varchar(30) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `fisiere`
--

CREATE TABLE IF NOT EXISTS `fisiere` (
  `id` int(11) NOT NULL auto_increment,
  `nume` varchar(200) NOT NULL,
  `cale` varchar(400) NOT NULL,
  `marime` float NOT NULL,
  `user` varchar(30) NOT NULL,
  `clasa` varchar(3) NOT NULL,
  `nr_d` bigint(20) NOT NULL,
  `cod` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `utilizatori`
--

CREATE TABLE IF NOT EXISTS `utilizatori` (
  `id` int(11) NOT NULL auto_increment,
  `nume` text NOT NULL,
  `prenume` text NOT NULL,
  `user` varchar(20) NOT NULL,
  `parola` varchar(100) NOT NULL,
  `clasa` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `rank` varchar(2) NOT NULL,
  `prieteni` text NOT NULL,
  `m_trimise` int(11) NOT NULL,
  `f_incarcate` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `thumb` text NOT NULL,
  `activat` bigint(20) NOT NULL,
  `resetat` bigint(20) NOT NULL,
  `blocat` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
