-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Gazda: localhost
-- Timp de generare: 15 Iul 2013 la 16:55
-- Versiune server: 5.0.92-community-log
-- Versiune PHP: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `colegi33_email`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `fisiere`
--

CREATE TABLE IF NOT EXISTS `fisiere` (
  `id` int(11) NOT NULL auto_increment,
  `nume` varchar(200) NOT NULL,
  `cale` varchar(400) NOT NULL,
  `marime` float NOT NULL,
  `user` varchar(30) NOT NULL,
  `clasa` varchar(3) NOT NULL,
  `nr_d` bigint(20) NOT NULL,
  `cod` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
