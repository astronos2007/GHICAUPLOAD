-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Gazda: localhost
-- Timp de generare: 15 Iul 2013 la 16:56
-- Versiune server: 5.0.92-community-log
-- Versiune PHP: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `colegi33_email`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `utilizatori`
--

CREATE TABLE IF NOT EXISTS `utilizatori` (
  `id` int(11) NOT NULL auto_increment,
  `nume` text NOT NULL,
  `prenume` text NOT NULL,
  `user` varchar(20) NOT NULL,
  `parola` varchar(100) NOT NULL,
  `clasa` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `rank` varchar(2) NOT NULL,
  `prieteni` text NOT NULL,
  `m_trimise` int(11) NOT NULL,
  `f_incarcate` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `thumb` text NOT NULL,
  `activat` bigint(20) NOT NULL,
  `resetat` bigint(20) NOT NULL,
  `blocat` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
