<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Lista mea de prieteni</h1><br />
<?
read_error();
read_succes();
if (friends_exist())
{
?>
<table border=5 style="border-radius: 10px; border-color:gray; zoom:90%; height:auto; width:auto;">
<thead>
<th style="text-align: center;">Nume de utilizator</th>
<th style="text-align: center;">Nume si prenume</th>
<th style="text-align: center;">Clasa</th>
<th style="text-align: center;">E-mail</th>
<th style="text-align: center;">Rang</th>
</thead>
<tbody>
<style type="text/css">
#link:link {color: blue; text-decoration:none;}
#link:active {color: orange; }
#link:visited {color: black; }
#link:hover {color: red; } 
</style>
<?
$q=query("SELECT prieteni FROM utilizatori WHERE user='".$_SESSION['user']."'");
$prieteni=explode("/", $q[0]);
foreach ($prieteni as $prieten)
    if (exist_user($prieten))
    {echo "<tr></tr><td style='text-align:center;'><img src='".get_thumb($prieten)."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/><b><span class='logo_colour'><a id='link' href='profil.php?id=".get_id_by_user($prieten)."'>".$prieten."</a></span></b></td><td style='text-align:center;'>".get_full_name_by_user($prieten)."</td>";
    if (rank($prieten)=='E') echo "<td style='text-align:center;'>".get_clasa_by_user($prieten)."</td>"; else echo "<td style='text-align:center;'>-</td>";
    echo "<td style='text-align:center;'>".get_email_by_user($prieten)."</td><td style='text-align:center;'>";
    if (rank($prieten)=='E') echo "<font color=darkgreen>elev</font>"; else if (rank($prieten)=='P') echo "<font color=magenta>profesor</font>"; else echo "<font color=red>admin</font>";
    echo '</td><td><a href="msgclick.php?id='.get_id_by_user($prieten).'"><button class="button" style="width: 100px;">TRIMITE MESAJ</button></a></td></tr>';
    }
?>
</tbody>
</table>
<?
}
else echo "<span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu aveti niciun prieten adaugat in lista.</span>";
?>
</div>
<?
include "footer.inc";
?>