<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
if (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A')
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";?>
<div class="content">
<h1>Fisiere tip "TOATE"</h1><br />
<br />
<?
read_error();
read_succes();
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    echo "<br/><span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu exista niciun fisier incarcat.</span>";
else if (!query("SELECT * FROM fisiere WHERE clasa='ALL'"))
    echo "<br/><span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu exista niciun fisier incarcat pentru clasa TOATE.</span>";
else{
?>
<br />
<table border=5 style="border-radius: 10px; border-color:gray; height:auto; zoom:90%; width:auto;">
<thead style="color: white;">
<th style="text-align: center;">Nume</th>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar<br/>descarcari</th>
<th colspan="3" style="text-align: center;">Comenzi</th>
</thead>
<tbody style="color:white">
<?
$query="SELECT * FROM fisiere WHERE clasa='ALL' ORDER BY id DESC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
    {
    if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
    else $marime=round($row['marime'], 2)." KB";
    echo "<tr><td style='text-align:center;'>".$row['nume']."</td><td style='text-align:center;'>".$marime."</td><td style='text-align:center;'>".$row['nr_d']."</td><td><a href=descarca.php?id=".$row['id']."><button class='button'>DESCARCA</button></a></td><td><a href='deletefile.php?id=".$row['id']."'><button class='button'>STERGE</button></a></td><td><a href='index.php?id=".$row['id']."'><button class='button'>TRIMITE</button></a></td></tr>";
    }
?>
</tbody>
</table>
<?
}
?>
</div>
<? include "footer.inc";?>