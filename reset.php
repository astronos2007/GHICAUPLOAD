<?
include "functii/functii.inc";
if (logat() || !isset($_GET['id_unic']) || !is_numeric($_GET['id_unic']) || ($_SERVER['REQUEST_METHOD']!='GET' && $_SERVER['REQUEST_METHOD']!='POST'))
    redirect("error.php?id=nepermis");
$id=mres($_GET['id_unic']);
$pass=mres($_POST['parola1']);
$pass2=mres($_POST['parola2']);
if (!query("SELECT * FROM utilizatori WHERE resetat=$id"))
    {
    opensession('error', '<font color="red">Cererea de resetare a parolei nu a fost facuta de pe contul dvs.!</font><br/>');
    redirect("forgotpass.php?id_unic=$id");
    }
if ($pass=='' || $pass2=='')
    {
    opensession('error', '<font color="red">Va rugam completati toate campurile!</font><br/>');
    redirect("forgotpass.php?id_unic=$id");
    }
if ($pass!=$pass2)
    {
    opensession('error', '<font color="red">Parolele nu coincid!</font><br/>');
    redirect("forgotpass.php?id_unic=$id");
    }
@mysql_query("UPDATE utilizatori SET parola=SHA1('$pass') WHERE resetat=$id");
@mysql_query("UPDATE utilizatori SET resetat=1 WHERE resetat=$id");
opensession('succes', 'Parola dvs. a fost actualizata cu succes!');
redirect("succes.php");
?>