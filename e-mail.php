<?
include "functii/functii.inc";
if ($_SERVER['REQUEST_METHOD']!='POST' || (!logat() && query("SELECT ip FROM ip WHERE ip=".gethostbyaddr($_SERVER['REMOTE_ADDR']))))
    redirect("error.php?id=nepermis");
$from=mres($_POST['from']);
$toemail=mres($_POST['toemail']);
$touser=mres($_POST['touser']);
$subject=mres($_POST['subject']);
$message=$_POST['message'];
$flag=1;
if ($touser=='' && $toemail=='')
    $flag=0;
if ($subject=='')
    $flag=0;
if ($message=='')
    $flag=0;
if ($flag==0)
    {
    opensession('error', '<font color="red">Va rugam completati toate campurile!</font><br/>');
    go_back();
    exit();
    }
if(($toemail!="") && ($touser!=""))
    {
    opensession('error', '<font color="red">Nu aveti voie sa completati ambele campuri destinatar!</font><br/>');
    go_back();
    exit();
    }
if($toemail!="" && !email_valid($toemail))
    {
    opensession('error', '<font color="red">E-mail invalid!</font><br/>');
    go_back();
    exit();
    }
else if($touser!="" && !exist_user($touser))
    {
    opensession('error', '<font color="red">Utilizatorul introdus nu exista in baza de date!</font><br/>');
    go_back();
    exit();
    }
if($from=="" && $touser=="")
    {
    opensession('error', '<font color="red">Daca trimiteti e-mail, va rugam completati campul "De la"!</font><br/>');
    go_back();
    exit();
    }
else if($from!="" && $touser!="") 
    {
    opensession('error', '<font color="red">Va rugam nu completati campul "De la" daca trimiteti PM!</font><br/>');
    go_back();
    exit();
    }
if ($from && bad_words($from) || bad_words($subject))
    {
    opensession('error', '<font color="red">Subiectul si campul "De la" nu trebuie sa contina cuvinte obscene!</font><br/>');
    go_back();
    exit();
    }
if($touser && $from=="")
    $from=$_SESSION['user'];
$extensii=get_extensii();
if ($_FILES['fisier']['name'])
    {
    if ($_FILES['fisier']['error'])
    {
    opensession('error', '<font color="red">'.$_FILES['fisier']['error'].'</font><br/>');
    go_back();
    exit();
    }
    $nume=mres($_FILES['fisier']['name']);
    $file_parts = @pathinfo($nume);
    $size=$_FILES['fisier']['size']/1024;
    if (!in_array(strtolower($file_parts['extension']), $extensii))
    {
    opensession('error', '<font color="red">Extensie neacceptata!</font><br/>');
    go_back();
    exit();
    }
    if (bad_words($nume))
    {
    opensession('error', '<font color="red">Numele fisierului contine cuvinte obscene!</font><br/>');
    go_back();
    exit();
    }
    if ($_FILES['fisier']['size']>get_dim_max()*1024000 && rank($_SESSION['user'])=='E')
    {
    opensession('error', '<font color="red">Fisierul este prea mare!</font>');
    go_back();
    exit();
    }
    if (query("SELECT * FROM fisiere WHERE nume='$nume'"))
    {
    opensession('error', '<font color="red">Va rugam redenumiti fisierul!</font>');
    go_back();
    exit();
    }
    @move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
    }
    $cod=rand(11111111111111, 99999999999999);
    @mysql_query("INSERT INTO fisiere (nume, cale, marime, user, clasa, cod) VALUES ('$nume', 'http://email.colegiulghica.ro/uploads/$nume', $size, '".$_SESSION['user']."', 'ALL', '$cod')");
    $cale='';
    if($nume)
        $cale='http://email.colegiulghica.ro/access.php?id='.$cod;
    if ($touser) 
        {
        if ($nume)
            {@mysql_query("INSERT INTO mesaje (user, mesaj, de_la, subiect, data, atasament, nume_att, citit) VALUES ('$touser', '$message', '$from', '$subject', NOW(), '$cale', '$nume', 0)");
            @mysql_query("INSERT INTO trimise (user, mesaj, destinatar, subiect, data, atasament, nume_att) VALUES ('".$_SESSION['user']."', '$message', '$touser', '$subject', NOW(), '$cale', '$nume')");
            }
        else 
            {@mysql_query("INSERT INTO mesaje (user, mesaj, de_la, subiect, data, atasament, nume_att, citit) VALUES ('$touser', '$message', '$from', '$subject', NOW(), '', '', 0)");
            @mysql_query("INSERT INTO trimise (user, mesaj, destinatar, subiect, data) VALUES ('".$_SESSION['user']."', '$message', '$touser', '$subject', NOW())");
            }
        $to=get_email_by_user($touser);
        mail($to, "Ati primit un PM pe GHICAUPLOAD", "Aceasta este o instiintare cum ca ati primit un mesaj privat pe GHICAUPLOAD, cu subiectul <<".$subject.">>.\n\nPentru a-l citi, vizitati http://email.colegiulghica.ro.\n\nEchipa GHICAUPLOAD.\nAcesta este un mesaj trimis automat. Va rugam nu raspundeti acestui e-mail.", "From: noreply@email.colegiulghica.ro");
        }
    else 
        {
        if ($nume) $message.=" \r\n\nATASAMENT: $cale";
        if (!email_valid($from)) $from=$from."@anonim.com";
        mail($toemail, $subject, $message, "From: $from");
        if (logat()) 
            if ($nume)
                @mysql_query("INSERT INTO trimise (user, mesaj, destinatar, subiect, data, atasament, nume_att) VALUES ('".$_SESSION['user']."', '$message', '$toemail', '$subject', NOW(), '$cale', '$nume')");
            else @mysql_query("INSERT INTO trimise (user, mesaj, destinatar, subiect, data) VALUES ('".$_SESSION['user']."', '$message', '$toemail', '$subject', NOW())");
        }
if (!logat()) @mysql_query("INSERT INTO ip (ip) VALUES ('".gethostbyaddr($_SERVER['REMOTE_ADDR'])."')");
if (logat()) increment_messages($_SESSION['user']);
opensession('succes', 'Mesajul a fost trimis cu succes!');
setcookie("53XvwsZeY8KtlxmB5lcF", "", time()-1);
redirect("succes.php");
?>