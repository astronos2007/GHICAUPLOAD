<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
if (rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A')
{
include "header.inc";
$ses=mres($_GET['ses']);
$clasa=mres($_POST['clasa']);
if (!$_SESSION['clasa'] && !$ses) opensession("clasa", "$clasa");
if ($ses=='none') $_SESSION['clasa']='';
else $clasa_s=$_SESSION['clasa'];
include "bar.inc";
include "right.inc";?>
<div class="content">
<h1>Fisiere incarcate</h1><br />
<?
read_error();
read_succes();
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    echo "<span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu exista niciun fisier incarcat.</span>";
else if ((!isset($_POST['clasa']) && !$_SESSION['clasa']) || $ses=='none')
{
?>
<br /><br /><br /><br /><br /><br />
<form class="form_settings" action="fisiere.php" method="POST">
Alegeti clasa pentru care sa fie afisate fisierele:
<select style="width:60px;" id="id" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while ($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL') echo "<option>".$row['clasa']."</option>";
?>
</select>
<input class="submit" style="width:65px;" type="submit" name="submit" value="Afisare"/>
</form>
<?}
else
{
if (query("SELECT * FROM fisiere WHERE clasa='$clasa'") || query("SELECT * FROM fisiere WHERE clasa='$clasa_s'"))
{
?>
<h2>Clasa: <?if ($clasa) echo $clasa; else echo $clasa_s;?></h2><br />
<table border=5 style="border-radius: 10px; border-color:gray; height:auto; zoom:80%; width:auto;">
<thead style="color: white;">
<th style="text-align: center;">Nume</th>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar<br />descarcari</th>
<th colspan="3" style="text-align: center;">Comenzi</th>
</thead>
<tbody style="color:white">
<?
if (!$_SESSION['clasa'])
    $query="SELECT * FROM fisiere WHERE clasa='$clasa' ORDER BY id DESC";
else $query="SELECT * FROM fisiere WHERE clasa='$clasa_s' ORDER BY id DESC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
        {
        if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
        else $marime=round($row['marime'], 2)." KB";
        echo "<tr><td style='text-align:center;'>".$row['nume']."</td><td style='text-align:center;'>".$marime."</td><td style='text-align:center;'>".$row['nr_d']."</td><td><a href=descarca.php?id=".$row['id']."><button class='button'>DESCARCA</button></a></td><td><a href='deletefile.php?id=".$row['id']."'><button class='button'>STERGE</button></a></td><td><a href='index.php?id=".$row['id']."'><button class='button'>TRIMITE</button></a></td></tr>";
        }
?>
</tbody>
</table>
<br />
<a href="fisiere.php?ses=none"><button class="button">Alege alta clasa</button></a>
<?}
else{
    opensession("error", "<font color=red>Nu exista niciun fisier incarcat pentru aceasta clasa!<br/></font>");
    redirect("fisiere.php?ses=none");
    }
}
?>
</div>
<?
include "footer.inc";
}
else
{
include "header.inc";
include "bar.inc";
include "right.inc";
$u=$_SESSION['user'];
$c=@query("SELECT clasa FROM utilizatori WHERE user='$u'");
$clasa=$c[0];
?>
<div class="content">
<h1>Fisiere incarcate</h1><br />
<h2>Clasa: <?if ($clasa) echo $clasa; else echo $clasa_s;?></h2><br />
<?
read_error();
read_succes();
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    echo "<br/><span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu exista niciun fisier incarcat.</span>";
else 
{
if (query("SELECT * FROM fisiere WHERE clasa='$clasa'") || query("SELECT * FROM fisiere WHERE clasa='$clasa_s'"))
{
?>
<table border=5 style="border-radius: 10px; border-color:gray; height:auto; zoom:80%; width:auto;">
<thead style="color: white;">
<th style="text-align: center;">Nume</th>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar<br />descarcari</th>
<th colspan="3" style="text-align: center;">Comenzi</th>
</thead>
<tbody style="color:white">
<?
if (!$_SESSION['clasa'])
    $query="SELECT * FROM fisiere WHERE clasa='$clasa' ORDER BY id DESC";
else $query="SELECT * FROM fisiere WHERE clasa='$clasa_s' ORDER BY id DESC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
        {
        if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
        else $marime=round($row['marime'], 2)." KB";
        echo "<tr><td style='text-align:center;'>".$row['nume']."</td><td style='text-align:center;'>".$marime."</td><td style='text-align:center;'>".$row['nr_d']."</td><td><a href=descarca.php?id=".$row['id']."><button class='button'>DESCARCA</button></a></td><td><a href='deletefile.php?id=".$row['id']."'><button class='button'>STERGE</button></a></td><td><a href='index.php?id=".$row['id']."'><button class='button'>TRIMITE</button></a></td></tr>";
        }
?>
</tbody>
</table>
<?}
else echo "<span style='background-color:#d0d0d0; border-radius:5px;'><font color=red>Nu exista niciun fisier incarcat pentru aceasta clasa!</font></span><br/>";
}
?>
</div>
<?
include "footer.inc";
}
?>