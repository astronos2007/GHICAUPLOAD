<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("error.php?id=nepermis");
$pass=mres($_POST['pass']);
$passnew=mres($_POST['passnew']);
$passnew2=mres($_POST['passnew2']);
if ($pass=='' || $passnew=='' || $passnew2=='')
    {
    opensession("error", "<font color='red'>Va rugam completati toate campurile!</font>");
    redirect("modifica.php?id=pass");
    }
if (SHA1($pass)!=get_parola_by_user($_SESSION['user']))
    {
    opensession("error", "<font color='red'>Parola curenta incorecta!</font>");
    redirect("modifica.php?id=pass");
    }
if (strlen($passnew)>40 || strlen($passnew)<6)
    {
    opensession('error', '<font color="red">Parola noua trebuie sa aiba intre 6 si 40 de caractere!</font><br/>');
    redirect("modifica.php?id=pass");  
    }
if ($passnew!=$passnew2)
    {
    opensession("error", "<font color='red'>Parolele nu coincid!</font>");
    redirect("modifica.php?id=pass");
    }
if (SHA1($passnew)==get_parola_by_user($_SESSION['user']))
    {
    opensession("error", "<font color='red'>Va rugam introduceti o parola noua diferita de cea curenta!</font>");
    redirect("modifica.php?id=pass"); 
    }
@mysql_query("UPDATE utilizatori SET parola='".SHA1($passnew)."' WHERE user='".$_SESSION['user']."'");
opensession("succes", "<font color=green>Parola a fost modificata cu succes!</font>");
if ($_COOKIE['66pWKLbxb1lWoWkvkd2T'])
    $_COOKIE['CqRh6hveZ7B0Din0eJhY']=SHA1($passnew);
redirect("admin.php");
?>