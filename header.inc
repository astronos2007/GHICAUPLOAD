<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta property="og:title" content="GHICAUPLOAD" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://email.colegiulghica.ro" />
<meta property="og:image" content="http://email.colegiulghica.ro/favicon.ico" />
<meta property="og:site_name" content="GHICAUPLOAD" />
<meta property="og:description" content="From anywhere, to everywhere, at anytime mail." />
<fb:like href="http://developers.facebook.com/" width="450" height="80" />
<!--<meta http-equiv="refresh" content="120"/>-->
<meta name="description" content="From anywhere, to everywhere, at anytime mail"/>
<meta name="keywords" content="mail, email, astromail, ghica, colegiulghica, colegiul ghica, CNGGV, e-mail, mail server, email host, send mail, trimite email, site mail, site email, email website"/>
<meta name="robot" content="index,follow"/>
<meta name="copyright" content="Copyright © 2013 Cosmin Crihan. Toate drepturile rezervate."/>
<meta name="author" content="Crihan Cosmin"/>
<meta name="language" content="Ro"/>
<meta name="revisit-after" content="7"/>
<script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="js/jquery-pack.js"></script>
<link rel="shortcut icon" type="image/ico" href="favicon.ico"/>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>GHICAUPLOAD</title>
</head>
<body style="<?
    $bg=generare_fundal();
    $url="../images/".$bg;
    $_SESSION['back']=$bg;
  ?>background: #FFF url(<?=$url;?>) no-repeat center fixed;">
  <div id="main">
    <header style="border-top-left-radius: 15px; border-top-right-radius:15px;">
      <div id="logo">
        <div id="logo_text">
          <h1><img src="images/stema.png" align="top" width="60" height="60"/>
          <a href="index.php">GHICA<span class="logo_colour">UPLOAD</span></a></h1>
          <h2>From anywhere, to everywhere, at anytime mail.</h2>
        </div>
      </div>