<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
if ($_SESSION['media'])
{
?>
<div class="content">
<h1>Calculator medii</h1>
<br /><br />
<h2>REZULTAT: <?echo $_SESSION['media'];
$_SESSION['media']='';
?></h2>
<br />
<a href="media.php"><button class="button" style="width: auto;">Calculeaza alta medie</button></a>
</div>
<?
}
else if (!$_POST && !$_SESSION['media'])
{
?>
<div class="content">
<h1>Calculator medii</h1>
<br /><br />
<form class="form_settings" action="media.php" method="POST">
Alegeti numarul de note: <input type="text" style="width: 50px;" name="nr_note" max="2" maxlength="2"/><br /><br />
<br />
<select id="id" style="width: auto;" name="teza">
<option>Cu teza</option>
<option>Fara teza</option>
</select>
<br />
<?
read_error();
echo "<br/>";
?><br />
<input class="submit" style="width: 70px;" type="submit" value="Continua"/>
</form>
</div>
<?
}
else if (isset($_POST['nr_note']) && isset($_POST['teza']))
{
    if ($_POST['nr_note']=='' || !is_numeric($_POST['nr_note']))
        {
        opensession("error", "<font color='red'>Introduceti un numar valid.</font>");
        redirect("media.php");
        }
$nr=mres($_POST['nr_note']);
if ($nr>99 || $nr<2)
{
 opensession("error", "<font color='red'>Introduceti un numar de note cuprins intre 2 si 99.</font>");
 redirect("media.php");  
}
$teza=mres($_POST['teza']);
?>
<div class="content">
<h1>Calculator medii</h1>
<br /><br />
<form class="form_settings" action="media.php" method="POST">
Introduceti notele:<br />
<?
for ($i=1; $i<=$nr; $i++)
    echo "Nota ".$i.": <input type='text' name='nota".$i."' style='width:50px;' max='2' maxlength='2'/><br/>";
?>
<br />
<?if ($teza=='Cu teza')
{
?>
Introduceti nota obtinuta la teza: <input type="text" name="nota_teza" style="width: 50px;" max="2" maxlength="2"/><br />
<?
}
?>
<input type="hidden" name="nr" value="<?=$nr;?>"/>
<input class="submit" style="width: 90px;" type="submit" value="Calculeaza"/>
</form>
</div>
<?
}
else if (isset($_POST['nota1']))
{
$nr=mres($_POST['nr']);
if ($_POST['nota_teza']=='' || !is_numeric($_POST['nota_teza']) || $_POST['nota_teza']<1 || $_POST['nota_teza']>10)
    {
    opensession("error", "<font color='red'>Reluati procedeul de calcul si introduceti note de tip numeric, cuprinse intre 1 si 10.</font>");
    redirect("media.php");
    }
for ($i=1; $i<=$nr; $i++)
    if ($_POST['nota'.$i]=='' || !is_numeric($_POST['nota'.$i]) || $_POST['nota'.$i]<1 || $_POST['nota'.$i]>10)
        {
        opensession("error", "<font color='red'>Reluati procedeul de calcul si introduceti note de tip numeric, cuprinse intre 1 si 10.</font>");
        redirect("media.php");
        }
$teza=mres($_POST['nota_teza']);
$suma=0;
for ($i=1; $i<=$nr; $i++)
    $suma+=mres($_POST['nota'.$i]);
if ($teza)
    $media=(($suma/$nr)*3+$teza)/4;
else $media=$suma/$nr;
opensession("media", round($media, 2));
redirect("media.php");
}
include "footer.inc";
?>