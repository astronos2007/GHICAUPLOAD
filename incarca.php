<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
if (rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A')
{
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Incarcare fisier</h1><br /><br />
<h2>Aici puteti incarca un fisier in baza de date.</h2><br />
<br />
<form class="form_settings" action="upload.php" method="POST" enctype="multipart/form-data">
Clasa pentru care incarcati fisierul: 
<select id="id" style="width: auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while ($row=@mysql_fetch_array($rez))
    {
    if ((rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A') && $row['clasa']=='ALL') echo "<option>".$row['clasa']."</option>";
    if ($row['clasa']!='ALL') echo "<option>".$row['clasa']."</option>";
    }
    ?>
</select>
<br /><br />
<input type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/><br />
<?
read_error();
?>
<br />
<input class="submit" style="width:70px;" type="submit" name="submit" value="Incarca" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/>
</form>
</div>
<?
include "footer.inc";
}
else
{
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Incarcare fisier</h1><br /><br />
<h2>Aici puteti incarca un fisier in baza de date.</h2><br />
<font color="red">ATENTIE!</font> Fisierul trebuie sa ocupe max. <?=get_dim_max();?> MB si sa aiba o extensie inofensiva!
<br />
<form class="form_settings" action="upload.php" method="POST" enctype="multipart/form-data">
Clasa pentru care incarcati fisierul: 
<?
$u=$_SESSION['user'];
$rez=@query("SELECT clasa FROM utilizatori WHERE user='$u'");
$clasa=$rez[0];
echo $clasa;
?>
<input type="hidden" name="clasa" value="<?=$clasa;?>"/>
<br /><input type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/>
<br />
<?
read_error();
?>
<br />
<input class="submit" style="width:70px;" type="submit" name="submit" value="Incarca" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/>
</form>
</div>
<?
include "footer.inc";
}
?>