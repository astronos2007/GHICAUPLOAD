<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || (!isset($_GET['id'])))
    redirect("error.php?id=nepermis");
$id=mres($_GET['id']);
if (!exist_user(get_user_by_id($id)))
    {
    opensession("error", "<font color='red'>Utilizatorul nu exista in baza de date!</font>");
    redirect("error.php");
    }
opensession("dest", $id);
redirect("");
?>