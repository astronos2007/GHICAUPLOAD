<?
include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
    $_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s'));
	$_SESSION['user_file_ext']= "";
}
$upload_dir = "upload_pic"; 
$upload_path = $upload_dir."/";
$large_image_prefix = "resize_";
$thumb_image_prefix = "thumbnail_";
$large_image_name = $large_image_prefix.$_SESSION['random_key']; 
$thumb_image_name = $thumb_image_prefix.$_SESSION['random_key'];
$max_file = get_dim_max_thumb();
$max_width = "500";
$thumb_width = "100";
$thumb_height = "100";
$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
$allowed_image_ext = array_unique($allowed_image_types);
$image_ext = "";
foreach ($allowed_image_ext as $mime_type => $ext) {
    $image_ext.= strtoupper($ext)." ";
}
$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];
if(!is_dir($upload_dir)){
	mkdir($upload_dir, 0777);
	chmod($upload_dir, 0777);
}
if (file_exists($large_image_location)){
	if(file_exists($thumb_image_location)){
		$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Imagine pictograma\"/>";
	}else{
		$thumb_photo_exists = "";
	}
   	$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION['user_file_ext']."\" alt=\"Imagine reala\"/>";
} else {
   	$large_photo_exists = "";
	$thumb_photo_exists = "";
}

if (isset($_POST["upload"])) {
$userfile_name = $_FILES['image']['name'];
$userfile_tmp = $_FILES['image']['tmp_name'];
$userfile_size = $_FILES['image']['size'];
$userfile_type = $_FILES['image']['type'];
$filename = basename($_FILES['image']['name']);
$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
    foreach ($allowed_image_types as $mime_type => $ext) {
			if($file_ext==$ext && $userfile_type==$mime_type){
				$error = "";
				break;
			}else
				$error = "<font color='red'>Doar imaginile de tip <strong>".$image_ext."</strong> sunt acceptate.</font><br />";
		}
		if ($userfile_size > ($max_file*1024000))
			$error.= "<font color='red'>Dimensiunea imaginii nu trebuie sa depaseasca ".$max_file." MB.</font>";
 }else 
	$error= "<font color='red'>Va rugam selectati o imagine pentru incarcare.</font>";
	if (strlen($error)==0){
		
		if (isset($_FILES['image']['name'])){
			$large_image_location = $large_image_location.".".$file_ext;
			$thumb_image_location = $thumb_image_location.".".$file_ext;
			$_SESSION['user_file_ext']=".".$file_ext;
			move_uploaded_file($userfile_tmp, $large_image_location);
			chmod($large_image_location, 0777);
			$width = getWidth($large_image_location);
			$height = getHeight($large_image_location);
			if ($width > $max_width){
				$scale = $max_width/$width;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}
            else{
				$scale = 1;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}
			if (file_exists($thumb_image_location)) {
				unlink($thumb_image_location);
			}
		}
		header("location:".$_SERVER["PHP_SELF"]);
		exit();
	}
}
if (isset($_POST["upload_thumbnail"]) && strlen($large_photo_exists)>0) {
	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];
	$scale = $thumb_width/$w;
	$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
	header("location:".$_SERVER["PHP_SELF"]);
	exit();
}


if ($_GET['a']=="delete" && strlen($_GET['t'])>0){
	$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
	$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
	if (file_exists($large_image_location)) {
		unlink($large_image_location);
	}
	if (file_exists($thumb_image_location)) {
		unlink($thumb_image_location);
	}
	header("location:".$_SERVER["PHP_SELF"]);
	exit(); 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" type="text/css" href="style.css" />
	<title>GHICAUPLOAD</title>
	<script type="text/javascript" src="js/jquery-pack.js"></script>
	<script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>
</head>
<body>
<?
if(strlen($large_photo_exists)>0){
	$current_large_image_width = getWidth($large_image_location);
	$current_large_image_height = getHeight($large_image_location);?>
<script type="text/javascript">
function preview(img, selection) { 
	var scaleX = <?php echo $thumb_width;?> / selection.width; 
	var scaleY = <?php echo $thumb_height;?> / selection.height; 
	
	$('#thumbnail + div > img').css({ 
		width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
		height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
} 

$(document).ready(function () { 
	$('#save_thumb').click(function() {
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("Selectati mai intai o zona din interiorul imaginii incarcate!");
			return false;
		}else{
			return true;
		}
	});
}); 

$(window).load(function () { 
	$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
});
</script>
<?}?>
<div class="content">
<h1><?if (!thumb_exist($_SESSION['user'])) echo "Incarcare"; else echo "Modificare";?> fotografie de profil</h1>
<?
if(strlen($error)>0)
	echo "<span style='background-color:#d0d0d0; border-radius:5px;'>".$error."</span>";
if(strlen($large_photo_exists)>0 && strlen($thumb_photo_exists)>0)
{
	echo $thumb_photo_exists."<br/>";
    echo "<br/><a href=\"setphoto.php\" target=\"_top\"><button class='button' style='width:180px;'>Seteaza ca fotografie de profil</button></a>";
	echo "<br/><a href=\"".$_SERVER["PHP_SELF"]."?a=delete&t=".$_SESSION['random_key'].$_SESSION['user_file_ext']."\"><button class='button' style='width:70px;'>Stergeti</button></a>";
	echo "<br/><a href=\"".$_SERVER["PHP_SELF"]."\"><button class='button' style='width:150px;'>Incarcati alta fotografie</button></a>";
    $_SESSION['random_key']= "";
    $_SESSION['user_file_ext']= "";
    opensession("photo", $thumb_image_location);
}
else{
		if(strlen($large_photo_exists)>0){?>
		<h2>Creare pictograma</h2>
		<div align="center">
			<img src="<?echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Creare pictograma" />
			<div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?echo $thumb_width;?>px; height:<?echo $thumb_height;?>px;">
				<img src="<?echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="position: relative;" alt="Previzualizare pictograma" />
			</div>
			<br style="clear:both;"/>
			<form class="form_settings" name="thumbnail" action="<?echo $_SERVER["PHP_SELF"];?>" method="post">
				<input type="hidden" name="x1" value="" id="x1" />
				<input type="hidden" name="y1" value="" id="y1" />
				<input type="hidden" name="x2" value="" id="x2" />
				<input type="hidden" name="y2" value="" id="y2" />
				<input type="hidden" name="w" value="" id="w" />
				<input type="hidden" name="h" value="" id="h" />
			<input class="submit" style="width: 150px; float:left;" type="submit" name="upload_thumbnail" value="Salvati pictograma" id="save_thumb" /><br /><br />
            </form>
		</div>
    <hr />
	<?}?>
	<h2>Incarcati o fotografie</h2>
	<form class="form_settings" name="photo" enctype="multipart/form-data" action="<? echo $_SERVER["PHP_SELF"];?>" method="post">
	Fotografie: <input type="file" name="image" size="30" /> 
    <input class="submit" style="width: 80px;" type="submit" name="upload" value="Incarca" />
	</form>
<? }
?>
</div>
</body>
</html>