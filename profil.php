<?include "functii/functii.inc";
if (!logat())
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<?
if (!isset($_POST['user']) && !isset($_GET['id']))
{
?>
<h1>Vizualizare profil</h1>
<br />
<form class="form_settings" action="profil.php" method="POST">
Introduceti numele de utilizator al carui profil doriti sa il vizualizati:
<br /><br />
<input type="text" name="user" max="30" maxlength="30"/>
<br/>
<?
read_error();
?>
<br /><input class="submit" style="width:90px;" type="submit" name="submit" value="Vizualizare"/>
</form>
<?
}
else if (isset($_POST['user']))
{
$user=mres($_POST['user']);
if ($user==$_SESSION['user'])
    redirect("admin.php");
if ($user=='')
    {
    opensession("error", "<font color='red'>Va rugam introduceti un nume de utilizator!</font>");
    redirect("profil.php");
    }
if (!get_id_by_user($user))
    {
    opensession("error", "<font color='red'>Utilizatorul introdus nu exista in baza de date!</font>");
    redirect("profil.php");    
    }
redirect("profil.php?id=".get_id_by_user($user));
}
if (isset($_GET['id']))
{
$id=mres($_GET['id']);
if (!is_numeric($id))
    {
    opensession("error", "<font color='red'>Nepermis!</font>");
    redirect("error.php");    
    }
$user=get_user_by_id($id);
if ($user=='')
    {
    opensession("error", "<font color='red'>Va rugam introduceti un nume de utilizator!</font>");
    redirect("profil.php");
    }
if ($user==$_SESSION['user'])
    redirect("admin.php");
?>
<h1>Profilul utilizatorului <b><i><?=$user;?></i></b></h1>
<span style="float: right;">
<a href="msgclick.php?id=<?=$id;?>"><button class="button" style="width: 100px;">TRIMITE MESAJ</button></a>
<?
if (!prieten($user)) echo '<a href="addfriend.php?id='.$id.'"><button class="button" style="width:100px;">ADAUGA PRIETEN</button></a>';
else echo '<a href="deletefriend.php?id='.$id.'"><button class="button" style="width:100px;">STERGE PRIETEN</button></a>';
?>
</span>
<br />
<?if (thumb_exist($user))
{?>
<img src="<?=get_thumb($user);?>" style="border-radius: 10px;" width='100' height='100'/>
<?}
else {
?><img src="images/empty.jpg" style="border-radius: 10px;" width='100' height='100'/>
<?}?>
<br />
<h3>Nume: <?=get_first_name_by_user($user);?></h3><br />
<h3>Prenume: <?=get_surname_by_user($user);?></h3><br />
<h3>E-mail: <b><?=get_email_by_user($user);?></b></h3><br />
<?if (rank($user)=='E') {?><h3>Clasa: <?=get_clasa_by_user($user);?></h3><br /><?}?>
<h3>Rang: <?if (rank($user)=='E') echo "ELEV"; else if (rank($user)=='P') echo "PROFESOR"; else echo "ADMIN";?></h3><br />
<h3>Numar de mesaje trimise: <?=get_nr_trimise($user);?></h3><br />
<h3>Numar de fisiere incarcate: <?=get_nr_fisiere($user);?></h3><br />
<h3>Cont blocat: <?if (is_blocat($user)) echo "DA"; else echo "NU";?></h3>
<?
}
?>
</div>
<?
include "footer.inc";
?>