<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])!='A' || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("error.php?id=nepermis");
$extensii=array("jpg", "jpeg", "png");
if (!$_FILES['fisier']['name'])
{
    opensession('error_add', '<font color="red">Nu ati incarcat niciun fisier!</font>');
    go_back();
    exit();
}
if ($_FILES['fisier']['error'])
{
    opensession('error_add', '<font color="red">'.$_FILES['fisier']['error'].'</font><br/>');
    go_back();
    exit();
}
$nume=mres($_FILES['fisier']['name']);
$file_parts = @pathinfo($nume);
$size=$_FILES['fisier']['size']/1024;
if (!in_array(strtolower($file_parts['extension']), $extensii))
{
    opensession('error_add', '<font color="red">Extensie neacceptata!</font><br/>');
    go_back();
    exit();
}
if ($_FILES['fisier']['size']>get_dim_max_thumb()*1024000 && (rank($_SESSION['user'])!='P' || rank($_SESSION['user'])!='A'))
{
    opensession('error_add', '<font color="red">Fisierul este prea mare! Dimensiunea imaginii nu trebuie sa depaseasca '.get_dim_max_thumb().' MB.</font>');
    go_back();
    exit();
}
$nume='images/back'.get_empty_back().".".$file_parts['extension'];
$back='back'.get_empty_back().".".$file_parts['extension'];
move_uploaded_file($_FILES['fisier']['tmp_name'], $nume);
$color=calculate_dominant_color($nume);
@mysql_query("INSERT INTO fundaluri (back, d_color) VALUES ('$back', '$color')");
opensession("succes", "<font color='green'>Imaginea de fundal a fost adaugata cu succes!</font>");
redirect("backs.php");
?>