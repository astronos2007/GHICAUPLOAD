<?
include "functii/functii.inc";
if ($_SERVER['REQUEST_METHOD']!='POST' || !logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("error.php?id=nepermis");
$clasa=mres($_POST['clasa']);
$nr_elevi=mres($_POST['nr_elevi']);
if ($clasa=='' || $nr_elevi=='')
    {
    opensession("errorm", "<font color='red'>Va rugam completati toate campurile!</font>");
    redirect("clase.php");
    }
if (!is_numeric($nr_elevi))
    {
    opensession("errorm", "<font color='red'>Completati numarul de elevi cu o valoare numerica!</font>");
    redirect("clase.php");
    }
if ($nr_elevi<1 || $nr_elevi>50)
    {
    opensession("errorm", "<font color='red'>Completati numarul de elevi cu o valoare numerica!</font>");
    redirect("clase.php");
    }
$q=query("SELECT COUNT(*) FROM utilizatori WHERE clasa='$clasa'");
if ($nr_elevi<$q[0])
    {
    opensession("errorm", "<font color='red'>Introduceti un numar de elevi mai mare sau egal decat ".$q[0]."!</font>");
    redirect("clase.php");
    }
@mysql_query("UPDATE clase SET nr_elevi=$nr_elevi WHERE clasa='$clasa'");
opensession("succes", "<font color='green'>Clasa a fost modficata cu succes!</font>");
redirect("clase.php");
?>