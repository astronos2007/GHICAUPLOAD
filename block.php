<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("error.php?id=nepermis");
if (!isset($_POST['user_b']) && !isset($_POST['user_d']))
{
include "header.inc";
include "bar.inc";
include "right.inc";?>
<div class="content">
<h1>Blocare/deblocare cont</h1><br />
<form class="form_settings" action="block.php" method="POST">
Introduceti numele de utilizator al carui cont doriti sa fie blocat: <br />
<br /><input type="text" max="30" maxlength="30" name="user_b"/>
<br />
<?
echo "<span style='background-color:#d0d0d0; border-radius:5px;'>".$_SESSION['errorb']."</span>";
$_SESSION['errorb']='';
?>
<br /><input style="width:90px;" class="submit" type="submit" name="submit" value="Blocheaza"/>
</form>
<br />
<form class="form_settings" action="block.php" method="POST">
Introduceti numele de utilizator al carui cont doriti sa fie deblocat:<br />
<br /><input type="text" max="30" maxlength="30" name="user_d"/>
<br />
<?
echo "<span style='background-color:#d0d0d0; border-radius:5px;'>".$_SESSION['errord']."</span>";
$_SESSION['errord']='';
?>
<br /><input style="width:90px;" class="submit" type="submit" name="submit" value="Deblocheaza"/>
</form>
</div>
<? include "footer.inc";
}
if (isset($_POST['user_b']))
{
    $user=mres($_POST['user_b']);
    if ($user=='')
        {
        opensession("errorb", "<font color='red'>Va rugam introduceti un nume de utilizator!</font>");
        redirect("block.php");
        }
    if ($user==$_SESSION['user'])
        {
        opensession("errorb", "<font color='red'>Nu va puteti bloca propriul cont!</font>");
        redirect("block.php");
        }
    if (rank($user)=='P')
        {
        opensession("errorb", "<font color='red'>Nu puteti bloca contul unui profesor!</font>");
        redirect("block.php");   
        }
    if (is_blocat($user))
        {
        opensession("errorb", "<font color='red'>Utilizatorul este deja blocat!</font>");
        redirect("block.php");   
        }
    @mysql_query("UPDATE utilizatori SET blocat=1 WHERE user='$user'");
    opensession("succes", "Contul utilizatorului ".$user." a fost blocat!");
    redirect("succes.php");
}
if (isset($_POST['user_d']))
{
    $user=mres($_POST['user_d']);
    if ($user=='')
        {
        opensession("errord", "<font color='red'>Va rugam introduceti un nume de utilizator!</font>");
        redirect("block.php");
        }
    if ($user==$_SESSION['user'])
        {
        opensession("errorb", "<font color='red'>Nu va puteti debloca propriul cont!</font>");
        redirect("block.php");
        }
    if (rank($user)=='P')
        {
        opensession("errord", "<font color='red'>Nu puteti debloca contul unui profesor!</font>");
        redirect("block.php");   
        }
    if (!is_blocat($user))
        {
        opensession("errord", "<font color='red'>Utilizatorul nu este blocat!</font>");
        redirect("block.php");   
        }
    @mysql_query("UPDATE utilizatori SET blocat=0 WHERE user='$user'");
    opensession("succes", "Contul utilizatorului ".$user." a fost deblocat!");
    redirect("succes.php");
}
?>