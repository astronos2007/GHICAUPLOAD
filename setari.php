<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Setari generale</h1>
<br />
<?
read_succes();
?>
<br />
<h4>E-mail-uri/mesaje blocate: <span style="color: white;"><?if (blocked_emails()) echo '<span style="color: white;">DA</span>'; else echo '<span style="color: white;">NU</span>';?>&nbsp; <a href="blockemails.php"><button class="button" style="width: 90px;"><?if (blocked_emails()) echo "DEBLOCARE"; else echo "BLOCARE";?></button></a></span></h4><br />
<h4>Incarcari blocate: <span style="color: white;"><?if (blocked_uploads()) echo '<span style="color: white;">DA</span>'; else echo '<span style="color: white;">NU</span>';?>&nbsp; <a href="blockuploads.php"><button class="button" style="width: 90px;"><?if (blocked_uploads()) echo "DEBLOCARE"; else echo "BLOCARE";?></button></a></span></h4><br />
<h4>Shoutbox blocat: <span style="color: white;"><?if (blocked_shoutbox()) echo '<span style="color: white;">DA</span>'; else echo '<span style="color: white;">NU</span>';?>&nbsp; <a href="blockshoutbox.php"><button class="button" style="width: 90px;"><?if (blocked_shoutbox()) echo "DEBLOCARE"; else echo "BLOCARE";?></button></a></span></h4><br />
<h4>Dimensiune maxima pentru fisierele incarcate: <span style="color: white;"><?=get_dim_max();?> MB</span>&nbsp; <a href="modifica.php?id=dim"><button class="button" style="width: 90px;">MODIFICA</button></a></h4><br />
<h4>Dimensiune maxima pentru fotografia de profil: <span style="color: white;"><?=get_dim_max_thumb();?> MB</span>&nbsp; <a href="modifica.php?id=dimthumb"><button class="button" style="width: 90px;">MODIFICA</button></a></h4><br />
<h4>Tipuri de extensii acceptate pentru fisierele incarcate: <span style="color: white; font-weight:bold;"><?=show_extensii();?></span></h4> <br />
<h4>Imagini de fundal: <a href="backs.php">Vezi fundalurile curente</a></h4>
</div>
<?
include "footer.inc";
?>