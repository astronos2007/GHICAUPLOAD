<?
include "functii/functii.inc";
if (logat())
    redirect("error.php?id=nepermis");
if ($_SERVER['REQUEST_METHOD']!='GET' && $_SERVER['REQUEST_METHOD']!='POST')
    redirect("error.php?id=nepermis");
if (isset($_GET['id_unic']) && $_SERVER['REQUEST_METHOD']=='GET')
{
$id=mres($_GET['id_unic']);
if (!query("SELECT * FROM utilizatori WHERE resetat=$id") || !is_numeric($id))
{
opensession('error', '<font color=red>Cererea de resetare a parolei nu a fost facuta de pe contul dvs.!</font><br/>');
redirect("forgot.php");
}
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Modificare parola</h1><br />
<form class="form_settings" action="reset.php?id_unic=<?=$id;?>" method="POST">
<span>Parola:</span><input type="password" name="parola1" size="25" /><br />
<span>Confirmare parola:</span><input type="password" name="parola2" size="25"/><br />
<?
read_error();
?>
<br />
<input class="submit" style="width:120px;" type='submit' name="submit" value="Reseteaza parola"/>
</form>
</div>
<? include "footer.inc";
}
else if (isset($_POST['email']) && $_SERVER['REQUEST_METHOD']=='POST')
{
$email=mres($_POST['email']);
if ($email=='')
    {
        opensession('error', '<font color="red">Va rugam completati e-mail-ul!</font><br/>');
        redirect("forgot.php");
    }
if (!email_valid($email))
    {
        opensession("error", "<font color='red'>E-mail invalid!</font><br/>");
        redirect("forgot.php");
    }
if (!query("SELECT * FROM utilizatori WHERE email='$email'"))
    {
        opensession('error', '<font color="red">E-mail-ul introdus nu exista in baza de date!</font><br/>');
        redirect("forgot.php");
    }
$id=rand(1000000000, 99999999999);
@mysql_query("UPDATE utilizatori SET resetat=$id WHERE email='$email'");
mail($email, "Resetare parola", "Ati optat pentru resetarea parolei contului dvs. de pe GHICAUPLOAD. Pentru a o reseta, accesati urmatorul link:\n http://email.colegiulghica.ro/forgotpass.php?id_unic=$id\n\nEchipa GHICAUPLOAD", "From:noreply@email.colegiulghica.ro");
opensession('succes', 'Veti primi un e-mail de confirmare pentru resetarea parolei.');
redirect("succes.php");
}
else redirect("error.php?id=nepermis");
?>