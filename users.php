<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Lista utilizatorilor GHICAUPLOAD</h1><br />
<table border=5 style="border-radius: 10px; border-color:gray; height:auto; zoom:80%; width:auto;">
<thead>
<th style="text-align: center;">Nume de utilizator</th>
<th style="text-align: center;">Ultima logare</th>
<th style="text-align: center;">Numar de<br />mesaje trimise</th>
<th style="text-align: center;">Numar de<br />fisiere incarcate</th>
<th style="text-align: center;">E-mail</th>
<th style="text-align: center;">Rang</th>
<th style="text-align: center;">Stare</th>
</thead>
<tbody>
<style type="text/css">
#link:link {color: blue; text-decoration:none;}
#link:active {color: orange; }
#link:visited {color: black; }
#link:hover {color: red; } 
</style>
<?
$q=@mysql_query("SELECT * FROM utilizatori ORDER BY user ASC");
while ($r=@mysql_fetch_array($q))
    {echo "<tr></tr><td style='text-align:center;'><img src='".get_thumb($r['user'])."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/><b><span class='logo_colour'><a id='link' href='profil.php?id=".get_id_by_user($r['user'])."'>".$r['user']."</a></span></b></td><td style='text-align:center;'>";
    if ((int)$r['last_login']!=0) echo $r['last_login'];
    else echo "niciodata";
    echo "</td><td style='text-align:center;'>".$r['m_trimise']."</td><td style='text-align:center;'>".$r['f_incarcate']."</td><td style='text-align:center;'>".$r['email']."</td><td style='text-align:center;'>";
    if (rank($r['user'])=='E') echo "<font color=darkgreen>elev</font>"; else if (rank($r['user'])=='P') echo "<font color=magenta>profesor</font>"; else echo "<font color=red>admin</font>";
    echo "</td><td style='text-align:center;'>";
    if (is_blocat($r['user'])) echo "blocat"; else if (!user_activ($r['user'])) echo "inactiv"; else echo "activ";
    echo "</td></tr>";
    }
?>
</tbody>
</table>
</div>
<?
include "footer.inc";
?>