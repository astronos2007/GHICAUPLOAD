<?
include "functii/functii.inc";
if ($_SERVER['REQUEST_METHOD']!='POST' || !logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("error.php?id=nepermis");
$clasa=mres($_POST['clasa']);
$nr_elevi=mres($_POST['nr_elevi']);
if ($clasa=='' || $nr_elevi=='')
    {
    opensession("errora", "<font color='red'>Va rugam completati toate campurile!</font>");
    redirect("clase.php");
    }
if (!preg_match("/^([0-9]){1,2}([A-J])$/", $clasa))
    {
    opensession("errora", "<font color='red'>Numele clasei este invalid!</font>");
    redirect("clase.php");  
    }
if (!is_numeric($nr_elevi))
    {
    opensession("errora", "<font color='red'>Completati numarul de elevi cu o valoare numerica!</font>");
    redirect("clase.php");
    }
if ($nr_elevi<1 || $nr_elevi>50)
    {
    opensession("errora", "<font color='red'>Numarul de elevi trebuie sa fie cuprins intre 1 si 50!</font>");
    redirect("clase.php");  
    }
if (query("SELECT clasa FROM clase WHERE clasa='$clasa'"))
    {
    opensession("errora", "<font color='red'>Clasa respectiva exista deja!</font>");
    redirect("clase.php");
    }
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('$clasa', $nr_elevi)");
@mysql_query("INSERT INTO anunturi (clasa, anunt) VALUES ('$clasa', '')");
opensession("succes", "<font color='green'>Clasa a fost adaugata cu succes!</font>");
redirect("clase.php");
?>