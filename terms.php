<?
include "functii/functii.inc";
include "header.inc";
include "bar.inc";
include "right.inc";?>
      <div class="content">
        <h1>Termeni si conditii</h1>
        <br />
        <ol type="1" style="text-align: justify;">
        <li>Site-ul nu va fi folosit decat in scopurile explicite pentru care a fost creat, si anume trimiterea de e-mail-uri, mesaje PM, incarcarea de fisiere. Este interzisa folosirea lui in scopuri comerciale sau frauduloase.</li>
        <li>Mesajele trimise vor avea un limbaj adecvat, nu vor contine injurii la adresa destinatarilor, amenintari, referinte la site-uri pornografice, droguri, alcool, arme, etc. NU se va folosi functia de trimitere e-mail pentru a promova reclame, a trimite mesaje spam, sau a deranja in vreun fel alte persoane.</li>
        <li>Fisierele incarcate sau trimise prin e-mail/PM nu vor avea continut de tip pornografic, sau orice are legatura cu drogurile, alcoolul, tutunul, armele, etc.</li>
        <li>NU sunt responsabil de niciun e-mail trimis / fisier incarcat de pe acest site. Orice utilizator ce foloseste site-ul este direct responsabil pentru ceea ce face, fapt pentru care a citit si acceptat acesti termeni de utilizare.</li>
        </ol>
        </div>
    </div>
<? include "footer.inc";?>