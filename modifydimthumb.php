<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("error.php?id=nepermis");
$dim=mres($_POST['dimthumb']);
if (!is_numeric($dim))
    {
    opensession("error", "<font color='red'>Introduceti o valoare numerica pentru noua dimensiune!</font>");
    redirect("modifica.php?id=dimthumb");
    }
if ($dim=='' || $dim<=0 || $dim>30)
    {
    opensession("error", "<font color='red'>Introduceti o valoare mai mare ca 0 si mai mica ca 30!</font>");
    redirect("modifica.php?id=dimthumb"); 
    }
@mysql_query("UPDATE setari SET max_thumb=$dim");
opensession("succes", "<font color='green'>Dimensiunea maxima pentru fotografia de profil a fost actualizata!</font>");
redirect("setari.php");
?>