<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("error.php?id=nepermis");
$dim=mres($_POST['dim']);
if (!is_numeric($dim))
    {
    opensession("error", "<font color='red'>Introduceti o valoare numerica pentru noua dimensiune!</font>");
    redirect("modifica.php?id=dim");
    }
if ($dim=='' || $dim<=0 || $dim>1024)
    {
    opensession("error", "<font color='red'>Introduceti o valoare mai mare ca 0 si mai mica ca 1024!</font>");
    redirect("modifica.php?id=dim"); 
    }
@mysql_query("UPDATE setari SET dim_max=$dim");
opensession("succes", "<font color='green'>Dimensiunea maxima pentru fisiere a fost actualizata!</font>");
redirect("setari.php");
?>