<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET')
    redirect("error.php?id=nepermis");
$id=mres($_GET['id']);
if ($id=='')
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Modificare
    <?if ($id=='name') echo " nume";
    else if ($id=='surname') echo " prenume";
    else if ($id=='pass') echo " parola";
    else if ($id=='user') echo " nume de utilizator";
    else if ($id=='dim') echo " dimensiune maxima pentru fisiere";
    else if ($id=='dimthumb') echo "dimensiune maxima pentru fotografia de profil";
    else redirect("error.php?id=nepermis");?>
</h1>
<br />
<?
if ($id=='user')
{
?>
<br /><br /><br /><br />
<form class="form_settings" action="modifyuser.php" method="POST">
Nume de utilizator nou: <input type="text" maxlength="25" max="25" name="user"/><br />
Confirmati numele de utilizator: <input type="text" maxlength="25" max="25" name="user2"/><br />
<?
read_error();
echo "<br/>";
?>
<input style="width:80px;" class="submit" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
else if ($id=='pass')
{
?>
<br /><br /><br /><br />
<form class="form_settings" action="modifypass.php" method="POST">
Parola curenta:<input type="password" maxlength="25" max="25" name="pass"/><br />
Parola noua:<input type="password" maxlength="25" max="25" name="passnew"/><br />
Confirma parola noua:<input type="password" maxlength="25" max="25" name="passnew2"/><br/>
<?
read_error();
echo "<br/>";
?>
<input style="width:80px;" class="submit" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
else if ($id=='name')
{
?>
<br /><br /><br /><br />
<form class="form_settings" action="modifyname.php" method="POST">
Noul nume:<input type="text" maxlength="25" max="25" name="name"/><br />
Noul prenume:<input type="text" maxlength="25" max="25" name="surname"/><br />
<?
read_error();
echo "<br/>";
?>
<input style="width:80px;" class="submit" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
else if ($id=='dim')
{
?>
<br /><br /><br /><br />
<form class="form_settings" action="modifydim.php" method="POST">
<h4>Dimensiune maxima actuala: <label style="color: white;"><?=get_dim_max();?> MB</label></h4> <br /><br />
Noua dimensiune maxima: <input type="text" maxlength="4" max="4" name="dim" style="width: 40px;"/> MB<br />
<?
read_error();
echo "<br/>";
?>
<input style="width:80px;" class="submit" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
else if ($id=='dimthumb')
{
?>
<br /><br /><br /><br />
<form class="form_settings" action="modifydimthumb.php" method="POST">
<h4>Dimensiune maxima actuala: <label style="color: white;"><?=get_dim_max_thumb();?> MB</label></h4> <br /><br />
Noua dimensiune maxima: <input type="text" maxlength="2" max="2" name="dimthumb" style="width: 20px;"/> MB<br />
<?
read_error();
echo "<br/>";
?>
<input style="width:80px;" class="submit" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
?>
</div>
<?
include "footer.inc";
?>