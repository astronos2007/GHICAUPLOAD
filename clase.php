<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])=='E')
    redirect("error.php?id=nepermis");
include "header.inc";
include "bar.inc";
include "right.inc";
$c=query("SELECT COUNT(*) FROM clase");
?>
<div class="content">
<h1>Gestiune clase</h1><br />
<?
read_succes();
?>
<div style="float: right; width: 200px; height: 100%;">
<form class="form_settings" action="addclass.php" method="POST">
<h2>Adauga o noua clasa</h2><br /><br />
Nume clasa: 
<input style="width:30px;" type="text" name="clasa" size="3" maxlength="3" max="3"/><br />
Numar de elevi: <input style="width:30px;" type="text" name="nr_elevi" size="2" maxlength="2" max="2"/><br />
<?
echo "<span style='background-color:#d0d0d0; border-radius:5px;'>".$_SESSION['errora']."</span>";
$_SESSION['errora']='';
?>
<br /><input class="submit" style="width: 60px;" type="submit" name="submit" value="Adauga"/>
</form>
<?
if ($c[0]!=0)
{
?>
<br />
<form class="form_settings" action="editclass.php" method="POST">
<h2>Modifica o clasa existenta</h2><br /><br />
Alegeti clasa: 
<select name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL')
        echo "<option>".$row['clasa']."</option>";
?>
</select><br />
Numar de elevi: <input style="width:30px;" type="text" name="nr_elevi" size="2" maxlength="2" max="2"/><br />
<?
echo "<span style='background-color:#d0d0d0; border-radius:5px;'>".$_SESSION['errorm']."</span>";
$_SESSION['errorm']='';
?>
<br /><input class="submit" style="width: 60px;" type="submit" name="submit" value="Modifica"/>
</form>
<?
}
?>
</div>
<?
if ($c[0]==0)
    echo "<br/><span style='background-color:#d0d0d0; border-radius:5px; color:red;'>Nu aveti nicio clasa adaugata.</span>";
else 
{
?>
<table border=5 style="border-radius: 10px; border-color:gray; height:auto; width: auto; zoom:90%;">
<thead style="color: white;">
<th style="text-align: center;">Clasa</th>
<th style="text-align: center;">Numar de elevi</th>
</thead>
<tbody style="color:white">
<?
$query="SELECT * FROM clase ORDER BY clasa ASC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL')
        echo "<tr><td style='text-align:center;'>".$row['clasa']."</td><td style='text-align:center;'>".$row['nr_elevi']."</td><td><a href='deleteclass.php?id=".$row['id']."'><button class='button'>STERGE</button></a></td></tr>";
?>
</tbody>
</table>
<?}
?>
</div>
<?
include "footer.inc";
?>