<?
include "functii/functii.inc";
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("error.php?id=nepermis");
if (!isset($_POST['check']) && $_SERVER['REQUEST_METHOD']!='POST')
{
include "header.inc";
include "bar.inc";
include "right.inc";
?>
<div class="content">
<h1>Avansare clase</h1>
<br />
<p>Aceasta functie va fi folosita la sfarsitul anului scolar, cand elevii vor trece intr-o clasa superioara, iar datele vor trebui actualizate in conformitate cu aceasta.</p>
<p><font color="red">ATENTIE: OPERATIE IREVERSIBILA! Odata cu apasarea butonului, clasele a 9-a vor deveni a 10-a, clasele a 10-a vor deveni a 11-a, clasele a 11-a vor deveni a 12-a, iar clasele a 12-a vor fi STERSE! Procesul este ireversibil: in cazul in care executati operatia din greseala datele NU vor putea fi recuperate!</font></p>
<form class="form_settings" action="avansare.php" method="POST">
<input type="checkbox" class="checkbox" name="check"/>Am citit avertismentul de mai sus si inteleg consecintele functiei de avansare a claselor.<br /><br />
<?read_error();
echo $_SESSION['error'];?><br />
<input class="submit" style="width:80px;" type="submit" name="submit" value="Avansare"/>
</form>
</div>
<?
include "footer.inc";
}
else
{
$check=mres($_POST['check']);
if (!$check)
    {
    opensession("error", "<font color='red'>Va rugam bifati optiunea de securitate!</font>");
    redirect("avansare.php");
    }

delete_messages_by_class("12A");
delete_messages_by_class("12B");
delete_messages_by_class("12C");
delete_messages_by_class("12D");
delete_messages_by_class("12E");
delete_messages_by_class("12F");
delete_messages_by_class("12G");
delete_messages_by_class("12H");
delete_messages_by_class("12I");
delete_messages_by_class("12J");

@mysql_query("DELETE FROM clase WHERE clasa='12A'");
@mysql_query("DELETE FROM clase WHERE clasa='12B'");
@mysql_query("DELETE FROM clase WHERE clasa='12C'");
@mysql_query("DELETE FROM clase WHERE clasa='12D'");
@mysql_query("DELETE FROM clase WHERE clasa='12E'");
@mysql_query("DELETE FROM clase WHERE clasa='12F'");
@mysql_query("DELETE FROM clase WHERE clasa='12G'");
@mysql_query("DELETE FROM clase WHERE clasa='12H'");
@mysql_query("DELETE FROM clase WHERE clasa='12I'");
@mysql_query("DELETE FROM clase WHERE clasa='12J'");

@mysql_query("DELETE FROM utilizatori WHERE clasa='12A'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12B'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12C'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12D'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12E'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12F'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12G'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12H'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12I'");
@mysql_query("DELETE FROM utilizatori WHERE clasa='12J'");

@mysql_query("DELETE FROM anunturi WHERE clasa='12A'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12B'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12C'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12D'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12E'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12F'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12G'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12H'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12I'");
@mysql_query("DELETE FROM anunturi WHERE clasa='12J'");

delete_file_by_class("12A");
delete_file_by_class("12B");
delete_file_by_class("12C");
delete_file_by_class("12D");
delete_file_by_class("12E");
delete_file_by_class("12F");
delete_file_by_class("12G");
delete_file_by_class("12H");
delete_file_by_class("12I");
delete_file_by_class("12J");

for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."A' WHERE clasa='".$i."A'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."B' WHERE clasa='".$i."B'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."C' WHERE clasa='".$i."C'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."D' WHERE clasa='".$i."D'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."E' WHERE clasa='".$i."E'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."F' WHERE clasa='".$i."F'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."G' WHERE clasa='".$i."G'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."H' WHERE clasa='".$i."H'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."I' WHERE clasa='".$i."I'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE utilizatori SET clasa='".($i+1)."J' WHERE clasa='".$i."J'");
    
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."A' WHERE clasa='".$i."A'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."B' WHERE clasa='".$i."B'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."C' WHERE clasa='".$i."C'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."D' WHERE clasa='".$i."D'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."E' WHERE clasa='".$i."E'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."F' WHERE clasa='".$i."F'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."G' WHERE clasa='".$i."G'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."H' WHERE clasa='".$i."H'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."I' WHERE clasa='".$i."I'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE anunturi SET clasa='".($i+1)."J' WHERE clasa='".$i."J'");
    
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."A' WHERE clasa='".$i."A'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."B' WHERE clasa='".$i."B'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."C' WHERE clasa='".$i."C'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."D' WHERE clasa='".$i."D'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."E' WHERE clasa='".$i."E'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."F' WHERE clasa='".$i."F'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."G' WHERE clasa='".$i."G'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."H' WHERE clasa='".$i."H'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."I' WHERE clasa='".$i."I'");
for ($i=11; $i>=9; $i--)
    @mysql_query("UPDATE fisiere SET clasa='".($i+1)."J' WHERE clasa='".$i."J'");
opensession("succes", "Clasele au fost avansate cu succes!");
redirect("succes.php");
}
?>