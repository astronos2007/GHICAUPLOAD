<?
include "functii/functii.inc";
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("error.php?id=nepermis");
$id=mres($_GET['id']);
$res=query("SELECT * FROM fisiere WHERE id=$id");
if (!$res)
    {
    opensession('error', 'Fisierul nu exista!');
    redirect("error.php");
    }
if (rank($_SESSION['user'])=='E' && $res['user']!=$_SESSION['user'])
    {
    opensession('error', '<font color="red">Nu aveti dreptul sa stergeti acest fisier!<br/></font>');
    redirect("fisiere.php");
    }
$cale="uploads/".$res['nume'];
unlink($cale);
if (file_class($id)=='ALL')
    $r="allfiles.php";
else $r="fisiere.php";
@mysql_query("DELETE FROM fisiere WHERE id=$id");
opensession('succes', '<font color="green">Fisierul a fost sters.</font><br/>');
redirect($r); 
?>